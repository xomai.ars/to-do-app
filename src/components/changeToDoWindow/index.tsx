import { useState } from "react"
import classes from "./ChangeToDoWindow.module.scss"
import { Controller } from "../../API";
import { Loading } from "../Loading";

type ChangeToDoWindowProps = {
    id: number;
    closeWindowCb: () => void
    oldData: {
        header:string;
        description: string;
        endTime: string
        imgSrc: string[]
    }
}

export const ChangeToDoWindow:React.FC<ChangeToDoWindowProps> = ({id, closeWindowCb, oldData}) => {
    const [header, setHeader] = useState<string>();
    const [description, setDescription] = useState<string>();
    const [endTime, setEndTime] = useState<string>();
    const [fileList, setFileList] = useState<FileList | null>(null);
    const [succesMessage, setSuccesMessage] = useState<string>();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const apiController = new Controller()

    return (
            <div onClick={() => closeWindowCb()} className={classes.container}>
                <form className={classes.form} onClick={(e) => e.stopPropagation()} >
                    <label>
                        Новый заголовок:
                        <input type="text" name="name" placeholder="Заголовок" value={header} onChange={(e) => setHeader(e.target.value)}/>
                    </label>
                    <label>
                        Новое описание:
                        <input type="text" name="name"placeholder="Описание" value={description} onChange={(e) => setDescription(e.target.value)} />
                    </label>
                    <label>
                        Новая дата завершения:
                        <input type="date" name="time" placeholder="Дата завершения" value={endTime} onChange={(e) => setEndTime(e.target.value)} />
                    </label>
                    <label>
                        Новые прикрепленные файлы:
                        <input type="file" name="file" multiple placeholder="Прикрепленные файлы" onChange={(e) => setFileList(e.target.files)} />
                    </label>
                </form>
                <button onClick={(e) => {
                    e.stopPropagation()
                    setSuccesMessage("Вы отправили данные, ожидайте конца загрузки")
                    setIsLoading(true)
                    apiController.changeToDo(fileList, header, description, endTime, id, () => {
                        setIsLoading(false)
                        closeWindowCb()
                    }, oldData)
                    }}>Сохранить</button>
                    <p style={{color: "green"}}>{succesMessage}</p>
                    {
                        isLoading 
                        ?
                            <Loading />
                        : 
                            null
                    }
        </div>
    )
}