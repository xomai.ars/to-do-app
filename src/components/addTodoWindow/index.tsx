import React, { useState } from "react"
import classes from "./AddToDoWindow.module.scss"
import { Controller } from "../../API";
import { Loading } from "../Loading";

type AddToDoWindowProps = {
    closeWindowCb: () => void
}

export const AddToDoWindow:React.FC<AddToDoWindowProps> = ({closeWindowCb}) => {
    const [header, setHeader] = useState<string>();
    const [description, setDescription] = useState<string>();
    const [endTime, setEndTime] = useState<string>();
    const [fileList, setFileList] = useState<FileList | null>(null );
    const [succesMessage, setSuccesMessage] = useState<string>();
    const [errrorMessage, setErrrorMessage] = useState<string>();
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const apiController = new Controller()
    const handleClick = (e:React.MouseEvent<HTMLButtonElement>) => {
        e.stopPropagation()
        if(header?.length === 0 || description?.length === 0 || endTime === undefined){
            setErrrorMessage("Все поля кроме файлов должны быть обязательно заполнены")
            return
        }
        setErrrorMessage("")
        setSuccesMessage("Вы отправили данные, ожидайте конца загрузки")
        setIsLoading(true)
        apiController.addNewToDo(fileList, header, description, endTime,() => {
            setIsLoading(false)
            closeWindowCb()
        });
    }
    return (
            <div  onClick={() => closeWindowCb()} className={classes.container}>
                <form className={classes.form} onClick={(e) => e.stopPropagation()}>
                    <label>
                        Заголовок:
                        <input type="text" name="name" placeholder="Заголовок" value={header} onChange={(e) => setHeader(e.target.value)}/>
                    </label>
                    <label>
                        Описание:
                        <input type="text" name="name"placeholder="Описание" value={description} onChange={(e) => setDescription(e.target.value)} />
                    </label>
                    <label>
                        Дата завершения:
                        <input type="date" name="time" placeholder="Дата завершения" value={endTime} onChange={(e) => setEndTime(e.target.value)} />
                    </label>
                    <label>
                        Прикрепленные файлы:
                        <input type="file" name="file" multiple placeholder="Прикрепленные файлы" onChange={(e) => setFileList(e.target.files)} />
                    </label>
                </form>
                    <button onClick={handleClick}>Отправить</button>
                        <p style={{color: "green"}}>{succesMessage}</p>
                        <p style={{color: "red"}}>{errrorMessage}</p>
                        {
                            isLoading 
                            ?
                                <Loading />
                            : 
                                null
                        }
        </div>
    )
}