import { useState } from "react";
import classes from "./ToDo.module.scss"
import { Controller } from "../../API";
import { ChangeToDoWindow } from "../changeToDoWindow";

type ToDoProps = {
    id: number;
    description: string;
    endTime: string;
    header: string;
    imgSrc: string[]
    isTooSoon: boolean
defaultIsMaked: boolean
}

export const ToDo:React.FC<ToDoProps> = ({id, description, endTime, header, imgSrc, isTooSoon, defaultIsMaked}) => {
    const [isDescriptionFull, setIsDescriptionFull] = useState<boolean>(false)
    const [isChangeWindowOpen, setIsChangeWindowOpen] = useState<boolean>(false)
    const [isMaked, setIsMaked] = useState<boolean>(defaultIsMaked)
    const apiController = new Controller()
    console.log(isMaked);
    return(
        <div className={`${classes.container}` +" " + `${isTooSoon ? classes.tooSoon : null}`+ " " + `${isMaked ? classes.maked : null}`}  onDoubleClick={() => setIsDescriptionFull(prev => !prev)}>
            <div className={classes.headerAndDescriptionContainer}>
                <h2>{header}</h2>
                <p>{isDescriptionFull ? description : description.slice(0, 100) + "..." }</p>
                <div className={classes.imageGallary}>
                    {imgSrc.map(item => {
                        console.log(item)
                        if(item.includes("image")){
                            return <img width={100} src={item}></img>
                        }else if(item.includes("video")){
                            return (
                            <video width={100} controls>
                                <source src={item}/>
                            </video>
                            )
                        }
                    })}
                </div>
                <h5>Время окончания: {endTime}</h5>
                
            </div>
            {
                isChangeWindowOpen 
                ?
                <ChangeToDoWindow 
                id={id} 
                closeWindowCb={() => setIsChangeWindowOpen(false)}
                oldData={{description, endTime, header, imgSrc}}
                />
                : 
                null
            }
            <div className={classes.buttons}>
                <button onClick={() => setIsChangeWindowOpen(prev => !prev)}>Редактировать</button>
                <button onClick={() =>{setIsMaked(prev => !prev);apiController.setToDoIsDone(id, {
header: header, description: description, endTime: endTime, imgSrc:imgSrc},!isMaked)}}>Выполнено</button>
                <button onClick={() => apiController.deleteToDo(id)}>Удалить запись</button>
            </div>
        </div>
    )
}
