import { collection, deleteDoc, doc, DocumentData, getDocs, setDoc } from "firebase/firestore";
import { getStorage, ref, uploadBytes, getDownloadURL } from "firebase/storage";
import { database } from "../backend";


export class Controller {
    getTodos = async (): Promise<DocumentData[] | undefined> => {
        try {
            const todosCol = collection(database, 'todos');
            const todoSnapshot = await getDocs(todosCol);
            const todoList = todoSnapshot.docs.map(doc => doc.data());
            return todoList
          } catch (e) {
            console.error("Error adding document: ", e);
            return undefined
          }
    }
    addNewToDo = async ( 
        file:FileList | null , 
        header:string | undefined, 
        description: string | undefined, 
        endTime: string | undefined, 
        closeModalCb: () => void) => {

        let mediaSrc: string[] = [];
        if(file !== null) { 
          for (let index = 0; index < file.length; index++) {
              const element = file[index];
              
              const fileArray = await element.arrayBuffer().then(res => res)
              const storage = getStorage();
              if(element.type.includes("image")){
                  const fileRef = ref(storage, `image/${element?.name}`);
                  await uploadBytes(fileRef, fileArray!).then((snapshot) => {
                  });
                  const mediaUploadedSrc = await getDownloadURL(ref(storage, `image/${element?.name }`)).then(res => res)
                  mediaSrc.push(mediaUploadedSrc)

              }else if(element.type.includes("video")){
                  const fileRef = ref(storage, `video/${element?.name}`);
                  await uploadBytes(fileRef, fileArray!).then((snapshot) => {
                  });
                  const mediaUploadedSrc = await getDownloadURL(ref(storage, `video/${element?.name }`)).then(res => res)
                  mediaSrc.push(mediaUploadedSrc)
              }
          }
        }
        const id = new Date().getTime()
        try {
            await setDoc(doc(database, "todos", `${id}` ), {
              id: id,
              header: header,
              description: description,
              endTime: endTime,
              imgSrc: mediaSrc
            });
            console.log("Процессы закончились")
            closeModalCb()
          } catch (e) {
            console.error("Error adding document: ", e);
          }
    }
    changeToDo = async (file:FileList | null, 
        header:string | undefined, 
        description: string | undefined, 
        endTime: string | undefined, 
        id: number,
        closeModalCb: () => void,
        oldData: {
            header:string;
            description: string;
            endTime: string
            imgSrc: string[]
        }) => {
        let mediaSrc: string[] = [];
        if(file !== null) { 
            for (let index = 0; index < file.length; index++) {
                const element = file[index];
                
                const fileArray = await element.arrayBuffer().then(res => res)
                const storage = getStorage();
                if(element.type.includes("image")){
                    const fileRef = ref(storage, `image/${element?.name}`);
                    await uploadBytes(fileRef, fileArray!).then((snapshot) => {
                    });
                    const mediaUploadedSrc = await getDownloadURL(ref(storage, `image/${element?.name }`)).then(res => res)
                    mediaSrc.push(mediaUploadedSrc)

                }else if(element.type.includes("video")){
                    const fileRef = ref(storage, `video/${element?.name}`);
                    await uploadBytes(fileRef, fileArray!).then((snapshot) => {
                    });
                    const mediaUploadedSrc = await getDownloadURL(ref(storage, `video/${element?.name }`)).then(res => res)
                    mediaSrc.push(mediaUploadedSrc)
                }
            }
        }
        try {
            await setDoc(doc(database, "todos", `${id}` ), {
              id: id,
              header: header ?? oldData.header,
              description: description ?? oldData.description,
              endTime: endTime ?? oldData.endTime,
              imgSrc: mediaSrc.length ? mediaSrc : oldData.imgSrc
            });
            console.log("Процессы закончились")
            closeModalCb()
          } catch (e) {
            console.error("Error adding document: ", e);
          }
    }
    deleteToDo = async (id: number) => {
        await deleteDoc(doc(database, "todos", `${id}`));
    }
    deleteAllToDo = async (todos:DocumentData[] | undefined) => {
        console.log(todos)
        todos?.map(async (item) => {
            await deleteDoc(doc(database, "todos", `${item.id}`))
        })
    }
    setToDoIsDone = async (id:number,oldData:  { header:string; 
            description: string;  endTime: string; imgSrc: string[]
        }, isDone:boolean) => {
try {
 await setDoc(doc(database, "todos", `${id}` ), { 
 id: id, 
              header:  oldData.header,
 description:  oldData.description,
 endTime: oldData.endTime, 
imgSrc: oldData.imgSrc,
isDone: isDone
            });
            console.log("Процессы закончились")
          } catch (e) {
            console.error("Error adding document: ", e);
          }
}
}
