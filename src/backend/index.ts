import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyDTg7ax18G6Wj_lzKcxik-x8D2qp8aoPCU",
    authDomain: "mytodoapp-ca7ab.firebaseapp.com",
    projectId: "mytodoapp-ca7ab",
    storageBucket: "mytodoapp-ca7ab.appspot.com",
    messagingSenderId: "724906319516",
    appId: "1:724906319516:web:88b9ebdb7c5a2141d3d759"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Cloud Firestore and get a reference to the service
export const database = getFirestore(app);