import { useEffect, useState } from 'react';
import classes from './app.module.scss';
import { DocumentData } from "firebase/firestore"; 
import { AddToDoWindow } from './components/addTodoWindow';
import { ToDo } from './components/toDo';
import { Controller } from './API';

function App() {
  const [todos, setTodos] = useState<DocumentData[] >()
  const [isAddToDoWindowOpen, setIsAddToDoWindowOpen] = useState<boolean>(false);
  const apiController = new Controller()
  
  useEffect(() => {
    apiController.getTodos().then(res => setTodos(res?.reverse()))
  }, [])
  return (
    <div>
      <header className={classes.header}>
        <h1>My to do app</h1>
      </header>
      <main>
        <div className={classes.actionButtonsContainer}>
          <button onClick={() => setIsAddToDoWindowOpen(true) }>Добавить задачу</button>
          <button onClick={() => apiController.deleteAllToDo(todos)}>Удалить все</button>
        </div>
        <div className={classes.todosWrapper}>
          <div className={classes.todosContainer}>
            {todos?.map(item => {
              const targetDateTimestamp = new Date(item.endTime).getTime()
              const todayTimestamp = new Date().getTime()
              let tooSoon: boolean = false
              if((targetDateTimestamp - todayTimestamp) <= 0){
               tooSoon = true
              }
              return (
                <ToDo key={item.id} 
                id={item.id} 
                description={item.description} 
                endTime={item.endTime} 
                header={item.header} 
                imgSrc={item.imgSrc}
defaultIsMaked={item.isDone}               
 isTooSoon={tooSoon}></ToDo>
              )
              })}
          </div>
          {
            isAddToDoWindowOpen ?
            <AddToDoWindow closeWindowCb={() => setIsAddToDoWindowOpen(false)}/>
            :
            null
          }
        </div>
      </main>
    </div>
  );
}

export default App;
